const nconf = require('nconf');

export const config = nconf.argv()
    .env()
    .file({ file: './config.json' });
