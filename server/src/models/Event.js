import Sequilize from 'sequelize';
import { times } from 'lodash/times';

const   log         = require('../libs/log')(module),
        config      = require('../libs/config');

const User = new Sequilize(
    'relay',
    'postgres',
    'postgres',
    {
        dialect: 'postgres',
        host: 'localhost'
    }
);

const UserSchema = Connection.define('user', {
    first_name: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    second_name: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    third_name: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    email: {
        type: Sequilize.STRING,
        allowNull: false,
        validate: {
            isEmail: true,
        }
    },
    type_id: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    file_id: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    phone: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    password_hash: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    auth_key: {
        type: Sequilize.STRING,
        allowNull: false,
    },
    status_id: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    created: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    created: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    updated: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
    deleted: {
        type: Sequilize.INTEGER,
        allowNull: false,
    },
});

User.sync({force: true}).then(() => {
    times(10, () => {
        return UserSchema.create({
            first_name: 'first_name',
            second_name: 'second_name',
            third_name: 'third_name',
            email: 'email',
            type_id: null,
            file_id: null,
            phone: '+79040557863',
            password_hash: 'asdasdasdasdsad',
            auth_key: 'asdasdasdasdsad',
            status_id: null,
            created: '12-04-17',
            created: '12-04-17',
            updated: '12-04-17',
            deleted: '12-04-17',
        });
    });
});

export default User;
//http://www.pvsm.ru/testirovanie/177790
//https://habrahabr.ru/post/193458/
