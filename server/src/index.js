import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import { config, getLogger } from './libs';
import pg from 'pg';

const app = express(),
      log = getLogger(module);

// Parsing json
//парсинг application/json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.text());
app.use(bodyParser.json({ type: 'application/json'}));

// запуск статического файлового сервера, который смотрит на
// папку app/ (в нашем случае отдает index.html)
app.use(express.static(path.join(__dirname, "app")));

app.get('/', (req, res) => {
    res.send('API is running');
    const client = new pg.Client(config.ur);
    //https://github.com/DmsChrisPena/babel-nodejs-setup/blob/master/package.json
    //https://www.youtube.com/watch?v=XZJ90BVu_Bk&t=516s
});

app.get('/api', function (req, res) {
    res.send('API is running');
});

const server = app.listen(config.get('port'), function(){
    log.info('Express server listening on port', config.get('port'));
});

/* // Подключение остальных роутов
require('./routes')(app);



app.use(function(req, res, next){
    res.status(404);
    log.debug('Not found URL: %s',req.url);
    res.send({ error: 'Not found' });
    return;
});

app.use(function(err, req, res, next){
    res.status(err.status || 500);
    log.error('Internal error(%d): %s',res.statusCode,err.message);
    res.send({ error: err.message });
    return;
});

app.get('/ErrorExample', function(req, res, next){
    next(new Error('Random error!'));
});

// экспортируем для тестирования
module.exports = {
    app: app,
    server: server,
};
 */
